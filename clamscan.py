"""This is a simple script to scan a single file with clamscan """
import os
from sys import (argv, exit as sExit)
import json
import subprocess

class Clamscaner():
    """A class to scan your files for viruses using clamscan"""

    def __init__(self, file_check):
        """Initiate the file variable to be used self.inreject_deny_accept()
and setup and run class methods after some file validation."""
        self.file = file_check
        if not os.path.isdir(self.file) and os.path.isfile(self.file):
            scan_result = self.scan_file(self.file)
            parsed_result = self.parse_scanned_file_to_dict(scan_result)
            verdict_json = self.reject_deny_accept(parsed_result)
            print(verdict_json)


    def scan_file(self, clamscan_file):
        """This function will scan a single file with cmanscan """
        with subprocess.Popen(['clamscan', clamscan_file],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE) as scan:

            scanned = scan.communicate()

        if scanned[1] != b'':
            ## this would be better suited to be written to a log file so if  
            ## errors are persistent they can be found and addressed
            sExit(f'Error: {scanned[1]}')

        return scanned[0]


    def parse_scanned_file_to_dict(self, scanned_file):
        """Parse scanned file output from clamscan to a dictionary object"""
        scanned_dict = {}
        scanned_file = scanned_file.decode()
        for line in scanned_file.split('\n'):
            element = line.split(':', maxsplit=1)
            if element[0] != '' and element[0][0] != '-':
                scanned_dict.update({element[0]: element[1].lstrip()})

        return scanned_dict


    def scanned_file_to_json(self, scanned_file_dict):
        """Convert the scan results object to json format"""
        return json.dumps(scanned_file_dict)


    def reject_deny_accept(self, parsed_dict):
        """Reject, deny, accept based on the infected variable in the parsed 
output from clamcan data"""
        if int(parsed_dict['Infected files']) > 0:
            ## reject, remove and deny file and possibly blacklist user's ip
            ## addr not fully tested! Should remove file if infected but the
            ## rest is up to the server admin to implement.
            os.remove(self.file)

        return self.scanned_file_to_json(parsed_dict)


if __name__ == '__main__':
    if len(argv) > 2:
        sExit('Can only scan one file at a time.')

    if not os.path.exists(argv[1]):
        sExit("File path is not valid or file doesn't exist.")

    Clamscaner(argv[1])

